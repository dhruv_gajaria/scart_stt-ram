README

"cross_val.py" uses cross validation with f1_score for various benchmarks.
"knn_fs_analysis" is used to find the best features using knn algorithm.
"mem_profile" is used to find the memory consumption for the program.
"test_train_split_2" is used to experiment the for multi-core simulation.

______________________________________
The labels are as follows:
0:- 10us
1:- 26.5us
2:-50us
3:-75us
4:-100us
5:-1ms 
_______________________________________

Please see the data folder to access the csv files used.