#knn feature selection
import time
averg='micro'
from sklearn.metrics import f1_score
from sklearn.metrics import make_scorer
score = make_scorer(f1_score,average=averg)

import numpy as np
#import pandas as pd
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import ShuffleSplit
from sklearn.neighbors import KNeighborsClassifier
#clf4 = KNeighborsClassifier(n_neighbors=17,weights='distance')
#clf4=KNeighborsClassifier(n_neighbors=17,weights='distance')
clf4=KNeighborsClassifier(n_neighbors=3)
data = np.genfromtxt('data_sram_energy_l2_fs.csv', delimiter=',')
from sklearn.model_selection import train_test_split
#data_test=np.genfromtxt('sorted_latency_gap.csv',delimiter=',')
time_list=[]
train_list=[]
test_list=[]
feature_list=[]
cv1= ShuffleSplit(test_size=0.2, random_state=0)

for i in range (59):
    n=1+i
    X=data[1:,4:-n]    
    Y=data[1:,-1]
    X_train,X_test,y_train,y_test= train_test_split(X,Y,test_size=0.5, random_state=0)    
    
    clf4 = KNeighborsClassifier(n_neighbors=3,weights='uniform')
    clf4.fit(X_train,y_train)
    a=time.clock()
    clf4.predict(X_test)
    b=time.clock()
    pred_time=(b-a)/(X_test.shape[0])
    print('KNN ',60-n,np.mean(cross_val_score(clf4, X, Y,scoring=score,cv=cv1)), np.std(cross_val_score(clf4, X, Y,scoring=score,cv=cv1)),pred_time)
    
    
