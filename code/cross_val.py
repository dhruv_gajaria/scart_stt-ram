# -*- coding: utf-8 -*-
"""
Created on Sat Apr 28 17:27:13 2018

@author: dhruv
"""


from sklearn.metrics import fbeta_score, make_scorer
#ftwo_scorer = make_scorer(fbeta_score, beta=2,average='micro')
from sklearn.metrics import f1_score
from sklearn.metrics import make_scorer
ftwo_scorer = make_scorer(f1_score, average='micro')
from sklearn.model_selection import ShuffleSplit
import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.model_selection import cross_val_score
from sklearn import svm
from sklearn.svm import LinearSVC
from sklearn.decomposition import PCA
data = np.genfromtxt('data_sram_latency_l2_fs.csv', delimiter=',')
X = data[1:,4:-1]

Y = data[1:,-1]

#X_r = pca.fit(X).transform(X)
cv1= ShuffleSplit(test_size=0.6, random_state=0)




lin = svm.SVC(kernel='linear')
clf=LinearSVC(multi_class='ovr')



lin = svm.SVC(kernel='linear',cache_size=1000)
clf=LinearSVC(multi_class='ovr')


print('Linear_SVC ',np.mean(cross_val_score(clf, X, Y,scoring='f1_micro',cv=cv1)),np.std(cross_val_score(clf, X, Y,scoring='f1_micro',cv=cv1)))


from sklearn import tree
clf3= tree.DecisionTreeClassifier()
print('Dec_tree ',np.mean(cross_val_score(clf3, X, Y,scoring='f1_micro',cv=cv1)),np.std(cross_val_score(clf3, X, Y,scoring='f1_micro',cv=cv1)))

from sklearn.ensemble import ExtraTreesClassifier
forest = ExtraTreesClassifier(n_estimators=20,
                              random_state=0)
print('Ensemble ',np.mean(cross_val_score(forest, X, Y,scoring='f1_micro',cv=cv1)), np.std(cross_val_score(forest, X, Y,scoring='f1_micro',cv=cv1)))


from sklearn.ensemble import RandomForestClassifier
forest_1 = RandomForestClassifier(max_depth=2, random_state=0)
print('Random_Forest ',np.mean(cross_val_score(forest_1, X, Y,scoring='f1_micro',cv=cv1)),np.std(cross_val_score(forest_1, X, Y,scoring='f1_micro',cv=cv1)))

from sklearn.neighbors import KNeighborsClassifier
clf4 = KNeighborsClassifier(n_neighbors=3)
print('KNN ',np.mean(cross_val_score(clf4, X, Y,scoring='f1_micro',cv=cv1)), np.std(cross_val_score(clf4, X, Y,scoring='f1_micro',cv=cv1)))

from sklearn.svm import SVC
clf5=SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
    decision_function_shape='ovr', degree=3, gamma='auto', kernel='rbf',
    max_iter=-1, probability=False, random_state=None, shrinking=True,
    tol=0.001, verbose=False)
print('non-Linear_SVC ',np.mean(cross_val_score(clf5, X, Y,scoring='f1_micro',cv=cv1)),np.std(cross_val_score(clf5, X, Y,scoring='f1_micro',cv=cv1)))

from sklearn.ensemble import BaggingClassifier
from sklearn.tree import DecisionTreeClassifier

cart = DecisionTreeClassifier()
num_trees = 100
model = BaggingClassifier()

print('bagging_classsifier ',np.mean(cross_val_score(model, X, Y,scoring='f1_micro',cv=cv1)),np.std(cross_val_score(model, X, Y,scoring='f1_micro',cv=cv1)))

from sklearn.ensemble import AdaBoostClassifier
clf=AdaBoostClassifier()

print('adaboost ',np.mean(cross_val_score(clf, X, Y,scoring='f1_micro',cv=cv1)),np.std(cross_val_score(clf, X, Y,scoring='f1_micro',cv=cv1)))

from sklearn.ensemble import GradientBoostingClassifier
clf1 = GradientBoostingClassifier()

print('gradient_boost ',np.mean(cross_val_score(clf1, X, Y,scoring='f1_micro',cv=cv1)),np.std(cross_val_score(clf1, X, Y,scoring='f1_micro',cv=cv1)))


