# -*- coding: utf-8 -*-
"""
Created on Thu May 03 19:30:54 2018

@author: dhruv
"""

from memory_profiler import profile

#import sys
import numpy as np
from sklearn.model_selection import train_test_split
#from sklearn import svm
#import time
#import os
#import psutil 

precision = 10
data = np.genfromtxt('sorted_latency.csv', delimiter=',')
X = data[:,0:-1]
#learning_Rate = 0.0212
Y = data[:,-1]
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.2)

#from sklearn.neighbors import KNeighborsClassifier
#clf4 = KNeighborsClassifier(n_neighbors=3)
from sklearn.linear_model import PassiveAggressiveClassifier
#clf5 = PassiveAggressiveClassifier(random_state=0)

@profile(precision=precision)
def ml_funct():
    gauss = PassiveAggressiveClassifier(random_state=0)
    gauss.fit(X_train, y_train)
    gauss.predict(X_test)
    #return mean

if __name__ == '__main__':
    ml_funct()

