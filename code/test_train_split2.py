# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 16:46:01 2018

@author: dhruvgajaria
"""
averg='micro'
import time
import numpy as np
import pandas as pd
#from sklearn.model_selection import train_test_split
from sklearn import svm
from sklearn.svm import LinearSVC
from sklearn.metrics import f1_score
from sklearn.metrics import make_scorer
score = make_scorer(f1_score,average=averg)
data = np.genfromtxt('data_sram_latency_l2_fs_train.csv', delimiter=',')
X_train = data[1:,4:-1]
svc_train=0
svc_test=0
svc_time=0
gauss_time=0
gauss_train=0
gauss_test=0
bern_time=0
bern_train=0
bern_test=0
dectree_test=0
dectree_time=0
dectree_train=0
forest_test=0
forest_time=0
forest_train=0
rand_test=0
rand_time=0
rand_train=0
knn_test=0
knn_time=0
knn_train=0
#learning_Rate = 0.0212
y_train = data[1:,-1]
data_test=np.genfromtxt('data_sram_energy_l2_fs_test.csv',delimiter=',')
X_test= data_test[1:,4:-1]
y_test=data_test[1:,-1]

lin = svm.SVC(kernel='linear')
clf=LinearSVC(multi_class='ovr')
clf.fit(X_train, y_train)
a=time.clock()
clf.predict(X_test)
b=time.clock()
#print(y)
q=(b-a)
svc_time=svc_time+q
svc_train=(1 - clf.fit(X_train, y_train).score(X_train, y_train))+svc_train
svc_test=(1 - clf.fit(X_train, y_train).score(X_test, y_test))+svc_test
#label_predict_svc=[]
ynew_svc = clf.predict(X_test)
svc_test=f1_score(y_test,ynew_svc,average=averg)
#label_predict_svc.append(ynew)

#print ("Testing time for individual sample linear SVC(ovr) is:",q)
#print ("training error for linear SVC(ovr) is:",1 - clf.fit(X_train, y_train).score(X_train, y_train))
#print ("testing error for linear SVC(ovr) is :",1 - clf.fit(X_train, y_train).score(X_test, y_test))

from sklearn.naive_bayes import GaussianNB
clf1 = GaussianNB()
clf1.fit(X_train, y_train)
a=time.clock()
clf1.predict(X_test)
b=time.clock()
#print(y)
q=(b-a)
gauss_time=gauss_time+q
gauss_train=gauss_train+(1 - clf1.fit(X_train, y_train).score(X_train, y_train))
gauss_test=(1 - clf1.fit(X_train, y_train).score(X_test, y_test))+gauss_test
ynew_gauss = clf1.predict(X_test)
gauss_test=f1_score(y_test,ynew_gauss,average=averg)
#print ("Testing time for individual sampl for Gauss_nb is:",q)
#print ("training error for Gauss_nb  is:",1 - clf1.fit(X_train, y_train).score(X_train, y_train))
#print ("testing error for Gauss_nb is :",1 - clf1.fit(X_train, y_train).score(X_test, y_test))


from sklearn.naive_bayes import BernoulliNB
clf2 = BernoulliNB()
#clf.fit(X, Y)
clf2.fit(X_train, y_train)
a=time.clock()
clf2.predict(X_test)
b=time.clock()
#print(y)
q=(b-a)
bern_time=bern_time+q
bern_train=bern_train+(1 - clf2.fit(X_train, y_train).score(X_train, y_train))
bern_test=bern_test+(1 - clf2.fit(X_train, y_train).score(X_test, y_test))
ynew_bernoulli = clf2.predict(X_test)
bern_test=f1_score(y_test,ynew_bernoulli,average=averg)
print ("Testing time for individual sample Bernoulli_NB is:",q)
#print ("training error for Bernoulli_NB  is:",1 - clf2.fit(X_train, y_train).score(X_train, y_train))
#print ("testing error for Bernoulli_NB is :",1 - clf2.fit(X_train, y_train).score(X_test, y_test))

from sklearn import tree
clf3= tree.DecisionTreeClassifier()
clf3.fit(X_train, y_train)
a=time.clock()
clf3.predict(X_test)
b=time.clock()
#print(y)
q=(b-a)
dectree_time=dectree_time+q
dectree_train=dectree_train+(1 - clf3.fit(X_train, y_train).score(X_train, y_train))
dectree_test=dectree_test+(1 - clf3.fit(X_train, y_train).score(X_test, y_test))
ynew_decision = clf3.predict(X_test)
dectree_test=f1_score(y_test,ynew_decision,average=averg)
#print ("Testing time for individual sample for for decision tree is:",q)
#print ("training error for decision tree  is:",1 - clf3.fit(X_train, y_train).score(X_train, y_train))
#print ("testing error for for decision tree is :",1 - clf3.fit(X_train, y_train).score(X_test, y_test))

from sklearn.ensemble import ExtraTreesClassifier
forest = ExtraTreesClassifier(n_estimators=200,random_state=0,class_weight='balanced_subsample')

forest.fit(X_train, y_train)
a=time.clock()
forest.predict(X_test)
b=time.clock()
#print(y)
q=(b-a)
forest_time=forest_time+q
forest_train=forest_train+(1 - forest.fit(X_train, y_train).score(X_train, y_train))
forest_test=forest_test+(1 - forest.fit(X_train, y_train).score(X_test, y_test))
ynew_ensemble = forest.predict(X_test)
forest_test=f1_score(y_test,ynew_ensemble,average=averg)
#print ("Testing time for individual sample for ensemble is:",q)
#print ("training error for ensemble  is:",1 - forest.fit(X_train, y_train).score(X_train, y_train))
#print ("testing error for ensemble is :",1 - forest.fit(X_train, y_train).score(X_test, y_test))

from sklearn.ensemble import RandomForestClassifier
forest_1 = RandomForestClassifier(max_depth=5, random_state=0)
forest_1.fit(X_train, y_train)
a=time.clock()
forest_1.predict(X_test)
b=time.clock()
#print(y)
q=(b-a)
rand_time=rand_time+q
rand_train=rand_train+(1 - forest_1.fit(X_train, y_train).score(X_train, y_train))
rand_test=rand_test+(1 - forest_1.fit(X_train, y_train).score(X_test, y_test))
ynew_random_forest = forest_1.predict(X_test)
rand_test=f1_score(y_test,ynew_random_forest,average=averg)
#print ("Testing time for individual sample for random forest is:",q)
#print ("training error for random forest  is:",1 - forest_1.fit(X_train, y_train).score(X_train, y_train))
#print ("testing error for random forest is :",1 - forest_1.fit(X_train, y_train).score(X_test, y_test))

from sklearn.neighbors import KNeighborsClassifier
#clf4 = KNeighborsClassifier(n_neighbors=17,weights='distance')
clf4=KNeighborsClassifier(n_neighbors=3)
clf4.fit(X_train, y_train)
a=time.clock()
clf4.predict(X_test)
b=time.clock()
#print(y)
q=(b-a)
knn_time=knn_time+q
knn_train=knn_train+(1 - clf4.fit(X_train, y_train).score(X_train, y_train))
knn_test=knn_test+(1 - clf4.fit(X_train, y_train).score(X_test, y_test))
ynew_knn = clf4.predict(X_test)
features_knn=clf4.kneighbors(X_test)
knn_test=f1_score(y_test,ynew_knn,average=averg)
#features_knn.toarray()

time_list=[]
test_list=[]
train_list=[]

myint=1
time_list.extend((svc_time,gauss_time,bern_time,dectree_time,forest_time,rand_time,knn_time))
final_time=[x/myint for x in time_list]
test_list.extend((svc_test,gauss_test,bern_test,dectree_test,forest_test,rand_test,knn_test))
final_test=[x/myint for x in test_list]
train_list.extend((svc_train,gauss_train,bern_train,dectree_train,forest_train,rand_train,knn_train))
final_train=[x/myint for x in train_list]
timedata_stats=pd.DataFrame(final_time,index=["svc","gauss","Bernoulli","decision_tree","ensemble","random_forest","knn"],columns=["testing_time"])
traindata_stats=pd.DataFrame(final_train,index=["svc","gauss","Bernoulli","decision_tree","ensemble","random_forest","knn"],columns=["train_error"])
testdata_stats=pd.DataFrame(final_test,index=["svc","gauss","Bernoulli","decision_tree","ensemble","random_forest","knn"],columns=["test_error"])

result = pd.concat([timedata_stats,traindata_stats,testdata_stats], axis=1)
print(result)

from sklearn.ensemble import BaggingClassifier
from sklearn.tree import DecisionTreeClassifier

cart = DecisionTreeClassifier()
num_trees = 100
model = BaggingClassifier()#base_estimator=cart, n_estimators=num_trees)
model.fit(X_train,y_train)
a=time.clock()
model.predict(X_test)
b=time.clock()
q=(b-a)
print(q)
y_bagging=model.predict(X_test)
bagging_test=f1_score(y_test,y_bagging,average=averg)
print(bagging_test)
#print('bagging_classsifier ',np.mean(cross_val_score(model, X, Y,scoring='f1_micro',cv=cv1)),np.std(cross_val_score(model, X, Y,scoring='f1_micro',cv=cv1)))

from sklearn.ensemble import AdaBoostClassifier
clf=AdaBoostClassifier()
#clf.fit(X_train,y_train)
#model = BaggingClassifier(base_estimator=cart, n_estimators=num_trees)
clf.fit(X_train,y_train)
a=time.clock()
clf.predict(X_test)
b=time.clock()
q=(b-a)
print(q)
y_boost=clf.predict(X_test)
boost_test=f1_score(y_test,y_boost,average=averg)
print(boost_test)
#print(y)
#print('adaboost ',np.mean(cross_val_score(clf, X, Y,scoring='f1_micro',cv=cv1)),np.std(cross_val_score(clf, X, Y,scoring='f1_micro',cv=cv1)))

from sklearn.ensemble import GradientBoostingClassifier
clf1 = GradientBoostingClassifier()
#clf1.fit(X_train,y_train)
#a=time.clock()
#clf1.predict(X_test)
#b=time.clock()
clf1.fit(X_train,y_train)
a=time.clock()
clf1.predict(X_test)
b=time.clock()
q=(b-a)
print(q)
y_boost1=clf1.predict(X_test)
boost_test=f1_score(y_test,y_boost1,average=averg)
print(boost_test)
#print('gradient_boost ',np.mean(cross_val_score(clf1, X, Y,scoring='f1_micro',cv=cv1)),np.std(cross_val_score(clf1, X, Y,scoring='f1_micro',cv=cv1)))

#print(y)

