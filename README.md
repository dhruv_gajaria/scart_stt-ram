# SCART: Predicting STT-RAM Cache Retention Times Using Machine Learning

Reference:-
SCART: Predicting STT-RAM Cache Retention Times Using Machine Learning
Dhruv Gajaria, Kyle Kuan, and Tosiron Adegbija
International Green and Sustainable Computing Conference (IGSC), Washington DC, October 2019

The folder "code" contains the useful codes we used for machine learning using sklearn. 
The folder "data" contains all the relevant data we used for the paper.

Please read README in both the folders for more information.
