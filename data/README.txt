README


"data_sram_energy_l2_fs.csv" and "data_sram_latency_l2_fs.csv"contains features arranged according to their importance value in decsending order for latency and energy respectively.
The feature importance decreases as we go from left to right. The last column contains the label.

The rest of the files contain the test and train data we used for multi-core simulation.
________________________________________________________________________________________________

The labels are as follows:
0:- 10us
1:- 26.5us
2:-50us
3:-75us
4:-100us
5:-1ms 